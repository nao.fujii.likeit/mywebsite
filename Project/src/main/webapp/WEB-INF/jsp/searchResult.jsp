
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>検索結果</title>
<!-- searchResult.cssの読み込み -->
<link href="css/searchResult.css" rel="stylesheet" type="text/css" />
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link" href="Toppage">検索結果</a>
				</li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link"
					href="Mypage?id=${userInfo.id}">マイページ</a></li>
				<li class="nav-item"><a class="nav-link" href="Cart">カート</a></li>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="UserList">ユーザ一覧</a></li>
				</c:if>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="ItemList">商品管理</a></li>
				</c:if>
				<li class="nav-item"><a class="btn btn-danger" href="Logout">ログアウト</a>
				</li>
			</ul>
		</nav>
	</header>
	<div class="container mt-5"></div>
	<div class="container">
		<div class="col-6 mx-auto row">
			<div class="input-field col s8 offset-s2 ">
				<form action="ItemSearch">
					<i class="material-icons prefix">商品検索</i> <input type="text"
						name="search_word" value="${Searchword}">
				</form>
			</div>
		</div>
	</div>
	<div class="container mt-5">
		<!-- 検索結果 -->
		<div class="col-4 mx-auto">
			<h4 class="ml-2">「${Searchword}」の検索結果</h4>
		</div>
		<div class="container mt-4">
			<div class="col-2 mx-auto mb-3">
				<h6 class="ml-2">検索結果${itemCount}件</h6>
			</div>
			<div class="container mt-5">
				<div class=" row">

					<div class="section">
						<!--   商品情報   -->
						<div class="row">
							<c:forEach var="itemList" items="${itemList}" varStatus="status">
								<div class="col s12 m3">
									<div class="card">
										<div class="card-image">
											<a
												href="Item?item_id=${itemList.item_id}&page_num=${pageNum}"><img
												src="img/${itemList.file_name}"></a>
										</div>
										<div class="card-content">
											<span class="card-title">${itemList.name}</span>
											<p>${itemList.formatPrice}円</p>
										</div>
									</div>
								</div>
								<c:if test="${(status.index + 1) % 4 == 0}">
						</div>
						<div class="row">
							</c:if>
							</c:forEach>
						</div>
					</div>
					<div class="container mt-1"></div>
					<div class="col-4 mx-auto row">
						<ul class="pagination">
							<!-- １ページ戻るボタン  -->
							<c:choose>
								<c:when test="${pageNum == 1}">
								</c:when>
								<c:otherwise>
									<li class="page-item"><a class="page-link"
										href="ItemSearch?search_word=${Searchword}&page_num=${pageNum - 1}"><i
											class="material-icons">前へ</i></a></li>
								</c:otherwise>
							</c:choose>

							<!-- ページインデックス -->
							<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}"
								end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}"
								step="1" varStatus="status">
								<li class="page-item"
									<c:if test="${pageNum == status.index }"> class="active" </c:if>><a
									class="page-link"
									href="ItemSearch?search_word=${Searchword}&page_num=${status.index}">${status.index}</a></li>
							</c:forEach>

							<!-- 1ページ送るボタン -->
							<c:choose>
								<c:when test="${pageNum == pageMax || pageMax == 0}">
								</c:when>
								<c:otherwise>
									<li class="page-item"><a class="page-link"
										href="ItemSearch?search_word=${Searchword}&page_num=${pageNum + 1}"><i
											class="material-icons">次へ</i></a></li>
								</c:otherwise>
							</c:choose>
						</ul>
					</div>
				</div>
</body>
</html>



