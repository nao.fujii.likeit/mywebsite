<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザ情報更新</title>
<!-- myPage.cssの読み込み -->
<link href="css/myPage.css" rel="stylesheet" type="text/css" />
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link" href="Toppage">ユーザ情報更新</a></li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link"
					href="Mypage?id=${userInfo.id}">マイページ</a></li>
				<li class="nav-item"><a class="nav-link" href="Cart">カート</a></li>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="Toppage">TopPage</a></li>
				</c:if>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="ItemList">商品管理</a></li>
				</c:if>
				<li class="nav-item"><a class="btn btn-danger" href="Logout">ログアウト</a>
				</li>
			</ul>
		</nav>
	</header>

	<div class="container mt-5">
		<!-- ユーザ情報 -->
		<div>
			<div class="col-3 mx-auto">
				<h3>ユーザ情報更新</h3>
			</div>
			<form method="post" action="#" class="form-horizontal">
				<div class="col-4 mx-auto userInfo">
					<table>
						<tbody>
							<tr>
								<td height="50">ログインID</td>
								<td>
									<p class="form-control-plaintext">${User.loginId}</p> <input
									type="hidden" name="loginId" class="form-control" id="loginId"
									value=${User.loginId}>
								</td>
							</tr>
							<tr>
								<td height="50">パスワード</td>
								<td><input type="password" name="password"
									class="form-control" id="password"></td>
							</tr>
							<tr>
								<td height="50">パスワード（確認用）</td>
								<td><input type="password" name="passwordConf"
									class="form-control" id="passwordConf"></td>
							</tr>
							<tr>
								<td height="50">ユーザ名</td>
								<td><input type="text" name="userName" class="form-control"
									id="userName" value=${User.userName}></td>
							</tr>
							<tr>
								<td width="100" height="50">名前</td>
								<td><input type="text" name="realName" class="form-control"
									id="realName" value=${User.realName}></td>
							</tr>
							<tr>
								<td height="50">住所</td>
								<td><input type="text" name="address" class="form-control"
									id="address" value=${User.address}></td>
							</tr>
						</tbody>
					</table>
					<p></p>
					<div class="col-7 mx-auto">
						<input type="submit" class="btn btn-outline-danger ml-5"
							value="更新">
					</div>
			</form>

		</div>
		<div class="container mt-5"></div>
		<div class="col-1 mx-auto">
			<a href="Mypage?id=${userInfo.id}" class="text-danger">戻る</a>
		</div>
	</div>