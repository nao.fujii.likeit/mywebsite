<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>商品削除</title>
<!-- myPage.cssの読み込み -->
<link href="css/myPage.css" rel="stylesheet" type="text/css" />
<!-- header.cssの読み込み -->
<link href="css/headerAdmin.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link" href="ItemList">【管理者】商品削除</a>
				</li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link"
					href="Mypage?id=${userInfo.id}">マイページ</a></li>
				<li class="nav-item"><a class="nav-link" href="Cart">カート</a></li>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="UserList">ユーザ一覧</a></li>
				</c:if>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="Toppage">TopPage</a></li>
				</c:if>
				<li class="nav-item"><a class="btn btn-primary" href="Logout">ログアウト</a>
				</li>
			</ul>
		</nav>
	</header>

	<div class="container mt-5">
		<!-- ユーザ削除 -->
		<div>

			<h2>商品削除</h2>
			<div class="container mt-5">
				<div class="container">
					<div class="row">
						<br class="col-6">
						<h5>商品名：${itemDel.name}</h5>
						</br>
						<div class="col-6">
							<img src="img/${itemDel.file_name}">
						</div>

						<div class="container mt-5"></div>
						<div class="col-5">
							<h5>本当に削除してよろしいでしょうか。</h5>
						</div>

						<form method="post" action="AdminItemDelete"
							class="form-horizontal">
							<input type="hidden" name="itemId" class="form-control"
								id="itemId" value=${itemDel.item_id}>

							<div class="container mt-5">
								<div class="row">
									<div class="col-sm-5">
										<a href="ItemList" class="btn btn-secondary">もどる</a>
									</div>

									<div class="col-sm-5">
										<button type="submit" value="削除" class="btn btn-secondary">削除する</button>
									</div>

								</div>
						</form>
						</form>
					</div>
				</div>
</body>

</html>