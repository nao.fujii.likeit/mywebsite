<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>商品マスタ商品更新</title>
<!-- header.cssの読み込み -->
<link href="css/headerAdmin.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- adminItem.cssの読み込み -->
<link href="css/adminItem.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link" href="ItemList">【管理者】商品情報更新</a>
				</li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link"
					href="Mypage?id=${userInfo.id}">マイページ</a></li>
				<li class="nav-item"><a class="nav-link" href="Cart">カート</a></li>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="UserList">ユーザ一覧</a></li>
				</c:if>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="Toppage">TopPage</a></li>
				</c:if>
				<li class="nav-item"><a class="btn btn-primary" href="Logout">ログアウト</a>
				</li>
			</ul>
		</nav>
	</header>

	<div class="container mt-5">
		<div class="col-3 mx-auto">
			<h2>商品情報更新</h2>
		</div>
		<!--   登録フォーム   -->
		<div class="container mt-5"></div>
		<form method="post" action="#" class="form-horizontal">
			<div class="form-group row">
				<div class="col-sm-10">
					<input type="hidden" name="item_id" class="form-control"
						id="item_id" value="${itemDB.item_id}">
				</div>
			</div>

			<div class="form-group row">
				<label for="itemName" class="col-sm-2 col-form-label">商品名</label>
				<div class="col-sm-10">
					<input type="text" name="name" class="form-control" id="itemName"
						value="${itemDB.name}">
				</div>
			</div>

			<div class="form-group row">
				<label for="detail" class="col-sm-2 col-form-label">商品説明</label>
				<div class="col-sm-10">
					<input type="text" name="detail" class="form-control" id="detail"
						value="${itemDB.detail}">
				</div>
			</div>

			<div class="form-group row">
				<label for="itemPrice" class="col-sm-2 col-form-label">価格</label>
				<div class="col-sm-10">
					<input type="text" name="price" class="form-control" id="itemPrice"
						value="${itemDB.price}">
				</div>
			</div>

			<div class="form-group row">
				<label for="file_name" class="col-sm-2 col-form-label">商品画像</label>
				<div class="row">
					<div class="col-6">
						<p>現在の画像</p>
						<img src="img/${itemDB.file_name}" alt="詳細">
					</div>
					<div class="container mt-5"></div>
					<div class="col-sm-10">
						<form method="post" enctype="multipart/form-data">
							<input type="file" name="file_name"> <br>
						</form>
					</div>
				</div>

				<div class="container mt-1">
					<div class="submit-button-area">
						<button type="submit" value="登録"
							class="btn btn-secondary btn-lg btn-block">登録</button>
					</div>
					<div class="container mt-5"></div>
					<div class="col-xs-4">
						<a href="ItemList">戻る</a>
					</div>
					<div class="container mt-5"></div>
		</form>
</body>

</html>