<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<html>

<head>
<meta charset="UTF-8">
<title>新規登録</title>
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link"
					href="NewUser">新規登録</a></li>
			</ul>
			<ul class="navbar-nav flex-row">

			</ul>
		</nav>
	</header>


	<div class="container mt-5">
		<div class="col-3 mx-auto">
			<h3>ユーザ登録</h3>
		</div>
		<!--   登録フォーム   -->
		<form method="post" action="#" class="form-horizontal">
			<div class="col-5 mx-auto">
				<table>
					<tbody>
						<tr height="50">
							<td class="center">ログインID</td>
							<td class="center"><input type="text" name="login_id"
								class="form-control" id="login_id">
						</tr>
						<tr height="50">
							<td class="center">ユーザ名</td>
							<td class="center"><input type="text" name="user_name"
								class="form-control" id="user_name">
						</tr>
						<tr height="50">
							<td class="center">パスワード</td>
							<td class="center"><input type="password" name="password"
								class="form-control" id="password">
						</tr>
						<tr height="50">
							<td class="center">パスワード(確認用)</td>
							<td class="center"><input type="password"
								name="passwordConf" class="form-control" id="passwordConf">
						</tr>
						<tr height="50">
							<td class="center" width="200">名前</td>
							<td class="center"><input type="text" name="real_name"
								class="form-control" id="real_name">
						</tr>
						<tr height="50">
							<td class="center">住所</td>
							<td class="center"><input type="text" name="address"
								class="form-control" id="address">
						</tr>
						<tr height="50">
							<td class="center">生年月日</td>
							<td class="center"><input type="date" name="birthDate"
								class="form-control" id="birthDate">
						</tr>
					</tbody>
				</table>
				<div class="col-3 mx-auto mt-3">
					<button type="submit" class="btn btn-outline-danger">登録</button>
				</div>
				<p></p>
				<div class="col-12 mx-auto">
					<a href="Login" class="text-danger">もどる</a>
				</div>
		</form>
</body>
</html>