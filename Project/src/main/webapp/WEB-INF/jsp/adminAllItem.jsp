<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>商品マスタ一覧</title>
<!-- searchResult.cssの読み込み -->
<link href="css/searchResult.css" rel="stylesheet" type="text/css" />
<!-- header.cssの読み込み -->
<link href="css/headerAdmin.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link" href="ItemList">【管理者】商品一覧</a></li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link"
					href="Mypage?id=${userInfo.id}">マイページ</a></li>
				<li class="nav-item"><a class="nav-link" href="Cart">カート</a></li>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="UserList">ユーザ一覧</a></li>
				</c:if>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="Toppage">TopPage</a></li>
				</c:if>
				<li class="nav-item"><a class="btn btn-primary" href="Logout">ログアウト</a></li>
			</ul>
		</nav>
	</header>

	<div class="container mt-5"></div>
	<!-- ユーザ一覧 -->
	<div>
		<div class="col-2 mx-auto">
			<h3>登録商品検索</h3>
		</div>
		<div class="container">
			<!-- 検索ボックス -->
			<div class="container mt-5"></div>
			<div class="search-form-area">
				<div class="panel-body">
					<form method="post" action="ItemList" class="form-horizontal">

						<div class="form-group row">
							<label for="itemName" class="col-sm-2 col-form-label">商品名</label>
							<div class="col-sm-10">
								<input type="text" name="itemName" class="form-control"
									id="itemName">
							</div>
						</div>

						<div class="form-group row">

							<label for="itemprice" class="col-sm-2 col-form-label">金額</label>

							<div class="row col-sm-10">
								<div class="col-sm-5">
									<input type="text" name="priceStart" class="form-control"
										id="price-start">
								</div>

								<div class="col-sm-1 text-center">~</div>
								<div class="col-sm-5">
									<input type="text" name="priceEnd" class="form-control"
										id="price-end">
								</div>
							</div>
						</div>
						<div class="text-right">
							<button type="submit" value="検索">検索</button>
						</div>
					</form>
				</div>
			</div>

			<!-- 新規登録ボタン -->
			<div class="create-button-area">
				<a class="btn btn-outline-primary btn-smlg" href="AdminNewItem">新規登録</a>
			</div>

			<!-- 検索結果一覧 -->
			<div class="container mt-5">
				<div class="col-2 mx-auto">
					<h3>商品一覧</h3>
				</div>
				<div class="section">
					<div class="row">

						<!--   購入履歴   -->
						<c:forEach var="itemList" items="${itemList}">
							<div class="col s12 m3">
								<div class="card" style="width: 15rem;">
									<img class="card-img-top" src="img/${itemList.file_name}"
										alt="購入品">
									<div class="card-body">
										<p class="card-title">${itemList.name}</p>
										<p class="card-text">${itemList.price}円</p>
									</div>
									<!-- 詳細・更新・削除 -->
									<div class="card-body">
										<a class="btn btn-primary btn-sm"
											href="AdminItemDetail?id=${itemList.item_id}">詳細</a> <a
											class="btn btn-success btn-sm"
											href="AdminItemUpdate?id=${itemList.item_id}">更新</a> <a
											class="btn btn-danger btn-sm"
											href="AdminItemDelete?id=${itemList.item_id}">削除</a>
									</div>
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>