<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>購入確認</title>
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link" href="Toppage">購入確認画面</a>
				</li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link"
					href="Mypage?id=${userInfo.id}">マイページ</a></li>
				<li class="nav-item"><a class="nav-link" href="Cart">カート</a></li>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="UserList">ユーザ一覧</a></li>
				</c:if>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="ItemList">商品管理</a></li>
				</c:if>
				<li class="nav-item"><a class="btn btn-danger" href="Logout">ログアウト</a>
				</li>
			</ul>
		</nav>
	</header>


	<div class="container mt-5"></div>
	<div class="col-2 mx-auto">
		<h3>購入確認</h3>
	</div>
	<div class="container mt-5">
		<div class="col-8 mx-auto">
			<div class="row">
				<table class="table">
					<thead class="thead-light">

						<tr>
							<th width="500">商品名</th>
							<th width="200">単価</th>
							<th width="200">小計</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="cartInItem" items="${cart}">
							<tr>
								<td>${cartInItem.name}</td>
								<td>${cartInItem.formatPrice}円</td>
								<td>${cartInItem.formatPrice}円</td>
							</tr>
						</c:forEach>
						<tr>
							<td>送料</td>
							<td>500円</td>
							<td>500円</td>
						</tr>

						<tr>
							<td></td>
							<td>合計</td>
							<td>${bdb.formatTotalPrice}円</td>
						</tr>
					</tbody>
					</thead>
				</table>
			</div>
			<div class="row">
				<div class="col-3 mx-auto">
					<input type="button" class="btn btn-outline-danger"
						onClick="location.href='BuyResult'" value="購入">
				</div>
			</div>
		</div>
	</div>
</body>

</html>