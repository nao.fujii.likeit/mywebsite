<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>TOP</title>
<!-- home.cssの読み込み -->
<link href="css/topPage.css" rel="stylesheet" type="text/css" />
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link" href="Toppage">TOP</a></li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link"
					href="Mypage?id=${userInfo.id}">マイページ</a></li>
				<li class="nav-item"><a class="nav-link" href="Cart">カート</a></li>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="UserList">ユーザ一覧</a></li>
				</c:if>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="ItemList">商品管理</a></li>
				</c:if>
				<li class="nav-item"><a class="btn btn-danger" href="Logout">ログアウト</a></li>
			</ul>
		</nav>
	</header>

	<!-- 検索欄 -->
	<div class="container mt-5"></div>
	<div class="container">
		<div class="col-6 mx-auto row">
			<div class="input-field col s8 offset-s2 ">
				<form action="ItemSearch">
					<i class="material-icons prefix">商品検索</i> <input type="text"
						name="search_word">
				</form>
			</div>
		</div>
	</div>


	<br>

	<div class="container mt-5"></div>
	<!-- 商品種類選択 -->
	<div>
		<div class="col-4 mx-auto row">
			<div class="col-6 center-align">
				<form action="ItemSearch">
					<button type='submit' name='search_word' value="ネックレス">ネックレス</button>
				</form>
			</div>
			<div class="col-6 center-align">
				<form action="ItemSearch">
					<button type='submit' name='search_word' value="ピアス">ピアス</button>
				</form>
			</div>
			<div class="container mt-3"></div>
			<div class="col-6 center-align">
				<form action="ItemSearch">
					<button type='submit' name='search_word' value="リング">リング</button>
				</form>
			</div>
			<div class="col-6 center-align">
				<form action="ItemSearch">
					<button type='submit' name='search_word' value="ブレスレット">ブレスレット</button>
				</form>
			</div>
		</div>
	</div>
	<div class="container mt-5"></div>

	<!-- おすすめ商品欄 -->
	<div class="container mt-5">
		<div class="">
			<h4 class="">おすすめ商品</h4>
		</div>
		<div class="section">
			<div class="row">

				<!--   おすすめ商品   -->
				<div class="row">
					<c:forEach var="itemList" items="${itemList}">
						<div class="col s12 m3">
							<div class="card">
								<div class="card-image">
									<a href="Item?item_id=${itemList.item_id}"><img
										src="img/${itemList.file_name}"></a>
								</div>
								<div class="card-content">
									<span class="card-title">${itemList.name}</span>
									<p>${itemList.formatPrice}円</p>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
</body>

</html>