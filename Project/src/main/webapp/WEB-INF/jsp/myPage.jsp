<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>マイページ</title>
<!-- myPage.cssの読み込み -->
<link href="css/myPage.css" rel="stylesheet" type="text/css" />
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link" href="Toppage">MyPage</a>
				</li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link"
					href="Mypage?id=${userInfo.id}">マイページ</a></li>
				<li class="nav-item"><a class="nav-link" href="Cart">カート</a></li>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="UserList">ユーザ一覧</a></li>
				</c:if>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="ItemList">商品管理</a></li>
				</c:if>
				<li class="nav-item"><a class="btn btn-danger" href="Logout">ログアウト</a>
				</li>
			</ul>
		</nav>
	</header>

	<div class="container mt-5">
		<!-- ユーザ情報 -->
		<div>
			<div class="col-3 mx-auto">
				<h3>ユーザ情報</h3>
			</div>
			<div class="col-10 mx-auto userInfo">
				<table class="table table-sm">
					<thead>

						<tr>
							<td>ログインID：</td>
							<td>
								<p class="form-control-plaintext">${User.loginId}</p>
							</td>
						</tr>
						<tr>
							<td>ユーザ名：</td>
							<td>
								<p class="form-control-plaintext">${User.userName}</p>
							</td>
						</tr>
						<tr>
							<td width="100">名前：</td>
							<td width="200">
								<p class="form-control-plaintext">${User.realName}</p>
							</td>
						</tr>
						<tr>
							<td width="100">住所：</td>
							<td>
								<p class="form-control-plaintext">${User.address}</p>
							</td>
						</tr>
						<tr>
							<td>生年月日：</td>
							<td>
								<p class="form-control-plaintext">${User.birthDate}</p>
							</td>
						</tr>
						<tr>
							<td>登録日時：</td>
							<td>
								<p class="form-control-plaintext">${User.createDate}</p>
							</td>
						</tr>
					</thead>
				</table>
			</div>
		</div>
		<div class="col-2 mx-auto">
			<a class="btn btn-outline-danger "
				href="UserUpdate?id=${userInfo.id}">修正する</a>
		</div>

		<!-- 購入履歴詳細 -->
		<div class="container mt-5">
			<div class="col-2 mx-auto">
				<h3>購入履歴</h3>
			</div>
			<div class="section">
				<div class="row">

					<!--   購入履歴   -->
					<c:forEach var="BuyDate" items="${BuyDate}">
						<div class="col s12 m3">
							<div class="card" style="width: 15rem;">
								<img class="card-img-top" src="img/${BuyDate.file_name}"
									alt="購入品">
								<div class="card-body">
									<p class="card-title">${BuyDate.name}</p>
									<p class="center">
										購入日：
										<fmt:formatDate value="${BuyDate.buyDate}" type="DATE"
											pattern="yyyy年MM月dd日" />
									</p>
									<p class="card-text">${BuyDate.price}円</p>
								</div>
							</div>
						</div>
					</c:forEach>

				</div>
			</div>

		</div>
</body>

</html>