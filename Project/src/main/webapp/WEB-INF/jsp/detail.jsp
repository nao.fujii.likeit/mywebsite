<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>商品詳細</title>
<!-- detail.cssの読み込み -->
<link href="css/detail.css" rel="stylesheet" type="text/css" />
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link" href="Toppage">商品詳細</a>
				</li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link"
					href="Mypage?id=${userInfo.id}">マイページ</a></li>
				<li class="nav-item"><a class="nav-link" href="Cart">カート</a>
				</li>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="UserList">ユーザ一覧</a></li>
				</c:if>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="ItemList">商品管理</a></li>
				</c:if>
				<li class="nav-item"><a class="btn btn-danger" href="Logout">ログアウト</a>
				</li>
			</ul>
		</nav>
	</header>

	<!-- おすすめ商品欄 -->
	<div class="container mt-5">
		<div class="col-2 mx-auto">
			<h3 class="ml-2">商品詳細</h3>
		</div>
		<br> </br>
		<div class="col-1 float-right">
			<form action="ItemAdd" method="POST">
				<input type="hidden" name="item_id" value="${item.item_id}">
				<button class="btn btn-outline-danger" type="submit" name="action">
					カートに追加</button>
			</form>
		</div>
		<div class="container mt-5"></div>
		<div class="row">
			<div class="col s6">
				<div class="card">
					<div class="card-image">
						<img src="img/${item.file_name}">
					</div>
				</div>
			</div>
			<div class="col-6">
				<div class="mt-5">
					<h3>${item.name}</h3>
				</div>
				<div>
					<h4>${item.formatPrice}円</h4>
				</div>
				<div>
					<h5 class="product-detail">${item.detail}</h5>
				</div>
			</div>
		</div>
		<div class="container mt-5"></div>
		<div class="col-1 mx-auto">
			<a href="Toppage" class="text-danger">もどる</a>
		</div>
		<div class="container mt-5"></div>
	</div>

</body>

</html>