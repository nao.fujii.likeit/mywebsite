<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザ詳細</title>
<!-- myPage.cssの読み込み -->
<link href="css/myPage.css" rel="stylesheet" type="text/css" />
<!-- header.cssの読み込み -->
<link href="css/headerAdmin.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link"
					href="UserList">【管理者】ユーザ情報詳細</a></li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link" href="Mypage?id=${userInfo.id}">マイページ</a>
				</li>
				<li class="nav-item"><a class="nav-link" href="Cart">カート</a>
				</li>
				 <c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="Toppage">TopPage</a></li>
				</c:if>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="ItemList">商品管理</a></li>
				</c:if>
				<li class="nav-item"><a class="btn btn-primary"
					href="Logout">ログアウト</a></li>
			</ul>
		</nav>
	</header>

	<div class="container mt-5">
		<!-- ユーザ一覧-->
		<div class="container mt-5">
			<!-- ユーザ情報 -->
			<div>
				<div class="col-4 mx-auto">
					<h3>ユーザ情報詳細参照</h3>
				</div>
				<div class="col-10 mx-auto userInfo">
					<table class="table table-sm">
						<thead>
							<tr>
								<td>ログインID：</td>
								<td>
									<p class="form-control-plaintext">${User.loginId}</p>
								</td>
							</tr>
							<tr>
								<td>ユーザ名：</td>
								<td>
									<p class="form-control-plaintext">${User.userName}</p>
								</td>
							</tr>
							<tr>
								<td width="100">名前：</td>
								<td width="200">
									<p class="form-control-plaintext">${User.realName}</p>
								</td>
							</tr>
							<tr>
								<td width="100">住所：</td>
								<td>
									<p class="form-control-plaintext">${User.address}</p>
								</td>
							</tr>
							<tr>
								<td>生年月日：</td>
								<td>
									<p class="form-control-plaintext">${User.birthDate}</p>
								</td>
							</tr>
							<tr>
								<td>登録日時：</td>
								<td>
									<p class="form-control-plaintext">${User.createDate}</p>
								</td>
							</tr>
							<tr>
								<td>最新更新日時：</td>
								<td>
									<p class="form-control-plaintext">${User.updateDate}</p>
								</td>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="col-2 mx-auto">
				<a href="UserList">戻る</a>
			</div>

			</form>


		</div>
	</div>

</body>

</html>