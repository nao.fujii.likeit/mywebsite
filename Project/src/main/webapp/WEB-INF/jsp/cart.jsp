
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>カート</title>
<!-- home.cssの読み込み -->
<link href="css/cart.css" rel="stylesheet" type="text/css" />
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link" href="Toppage">カート</a>
				</li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link"
					href="Mypage?id=${userInfo.id}">マイページ</a></li>
				<li class="nav-item"><a class="nav-link" href="Cart">カート</a></li>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="UserList">ユーザ一覧</a></li>
				</c:if>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="ItemList">商品管理</a></li>
				</c:if>
				<li class="nav-item"><a class="btn btn-danger" href="Logout">ログアウト</a>
				</li>
			</ul>
		</nav>
	</header>
	<div class="container mb-5">
		<div class="col-2 mx-auto mt-5">
			<h3 class="ml-4">カート</h3>
			<br> </br>
		</div>
	</div>


	<div class="container">
		<div class="row center">${cartActionMessage}</div>
		<div class="row center"></div>
		<div class="section">
			<form action="ItemDelete" method="POST">
				<div class="row">
					<div class="col s12">
						<div class="col s6 center-align">
							<button class="btn waves-effect waves-light col s6 offset-s3 "
								type="submit" name="action">削除</button>
						</div>
						<div class="col s6 center-align">
							<a href="Buy"
								class="btn waves-effect waves-light col s6 offset-s3">購入する</a>
						</div>
					</div>
				</div>
				<div class="row">
					<c:forEach var="item" items="${cart}" varStatus="status">
						<div class="col s12 m3">
							<div class="card">
								<div class="card-image">
									<a href="Item?item_id=${item.item_id}"><img
										src="img/${item.file_name}"> </a>
								</div>
								<div class="card-content">
									<span class="card-title">${item.name}</span>
									<p>${item.formatPrice}円</p>
									<p>
										<input type="checkbox" id="${status.index}"
											name="delete_item_id_list" value="${item.item_id}" /> <label
											for="${status.index}">削除</label>
									</p>
								</div>
							</div>
						</div>
						<c:if test="${(status.index+1) % 4 == 0 }">
				</div>
				<div class="row">
					</c:if>
					</c:forEach>

				</div>
				<div class="container mt-5"></div>
				<div class="col-2 mx-auto">
					<a href="Toppage" class="text-danger">買い物を続ける</a>
				</div>
				<div class="container mt-5"></div>
			</form>
		</div>
	</div>
</body>

</html>