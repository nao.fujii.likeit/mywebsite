<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>商品マスタ情報詳細</title>
<!-- detail.cssの読み込み -->
<link href="css/detail.css" rel="stylesheet" type="text/css" />
<!-- header.cssの読み込み -->
<link href="css/headerAdmin.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link" href="ItemList">【管理者】商品情報詳細</a>
				</li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link"
					href="Mypage?id=${userInfo.id}">マイページ</a></li>
				<li class="nav-item"><a class="nav-link" href="Cart">カート</a></li>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="UserList">ユーザ一覧</a></li>
				</c:if>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="Toppage">TopPage</a></li>
				</c:if>
				<li class="nav-item"><a class="btn btn-primary" href="Logout">ログアウト</a>
				</li>
			</ul>
		</nav>
	</header>

	<!-- おすすめ商品欄 -->
	<div class="container mt-5"></div>
	<div class="col-2 mx-auto">
		<h3 class="ml-2">商品詳細</h3>
	</div>
	<br></br>

	<div class="col-2 float-right">
		<a class="btn btn-outline-primary"
			href="AdminItemUpdate?id=${itemDetail.item_id}">更新する</a>
	</div>
	<div class="container mt-5"></div>
	<div class="row">
		<div class="col-5">
			<div class="card">
				<div class="card-image">
					<img src="img/${itemDetail.file_name}">
				</div>
			</div>
		</div>
		<div class="col-6">
			<div class="mt-5">
				<h3>${itemDetail.name}</h3>
			</div>
			<div>
				<h4>${itemDetail.price}円</h4>
			</div>
			<div>
				<h5 class="product-detail">${itemDetail.detail}</h5>
			</div>
			<div class="container mt-5"></div>
			<div>
				<p class="product-detail">登録日時：${itemDetail.createDate}</p>
			</div>
			<div>
				<p class="product-detail">更新日時：${itemDetail.updateDate}</p>
			</div>
		</div>
	</div>
	<div class="container mt-5"></div>
	<div class="col-1 mx-auto">
		<a href="ItemList" class="text-primary">もどる</a>
	</div>
	<div class="container mt-5"></div>
</body>

</html>