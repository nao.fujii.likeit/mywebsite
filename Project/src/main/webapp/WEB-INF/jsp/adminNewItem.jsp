<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>商品マスタ商品登録</title>
<!-- header.cssの読み込み -->
<link href="css/headerAdmin.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link" href="ItemList">【管理者】新規商品登録</a></li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link"
					href="Mypage?id=${userInfo.id}">マイページ</a></li>
				<li class="nav-item"><a class="nav-link" href="Cart">カート</a></li>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="UserList">ユーザ一覧</a></li>
				</c:if>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="Toppage">TopPage</a></li>
				</c:if>
				<li class="nav-item"><a class="btn btn-primary" href="Logout">ログアウト</a></li>
			</ul>
		</nav>
	</header>

	<div class="container mt-5">
		<div class="col-3 mx-auto">
			<h2>新規商品登録</h2>
		</div>
		<!--   登録フォーム   -->
		<form method="post" action="#" class="form-horizontal">
			<div class="col-5 mx-auto">
				<table>
					<tbody>
						<tr height="70">
							<td class="center">商品名</td>
							<td class="center"><input type="text" name="name"
								class="form-control" id="name">
						</tr>
						<tr height="70">
							<td class="center">商品説明</td>
							<td class="center"><input type="text" name="detail"
								class="form-control" id="detail">
						</tr>
						<tr height="70">
							<td class="center">価格</td>
							<td class="center"><input type="text" name="price"
								class="form-control" id="price">
						</tr>
						<tr height="70">
							<td class="center">商品画像</td>
							<td class="center">
								<form method="post" enctype="multipart/form-data">
									<input type="file" name="file_name"> <br>
								</form>
						</tr>
				</table>
				<div class="container mt-5">
					<div class="col-3 mx-auto mt-3">
						<button type="submit" class="btn btn-outline-primary">登録</button>
					</div>

					<div class="col-xs-4">
						<a href="ItemList">戻る</a>
					</div>
		</form>
</body>

</html>