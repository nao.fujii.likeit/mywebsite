
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>購入完了</title>
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link" href="Toppage">購入完了画面</a>
				</li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link"
					href="Mypage?id=${userInfo.id}">マイページ</a></li>
				<li class="nav-item"><a class="nav-link" href="Cart">カート</a></li>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="UserList">ユーザ一覧</a></li>
				</c:if>
				<c:if test="${userInfo.loginId == 'admin'}">
					<li class="nav-item"><a class="btn btn-outline-light"
						href="ItemList">商品管理</a></li>
				</c:if>
				<li class="nav-item"><a class="btn btn-danger" href="Logout">ログアウト</a>
				</li>
			</ul>
		</nav>
	</header>


	<div class="container mt-5">
		<div class="col-4 mx-auto">
			<h4>購入が完了しました</h4>
		</div>
		<br> </br>
		<div>
			<div class="col-6 mx-auto row">
				<div class="col-6 center-align">
					<a href="Toppage" class="text-danger">引き続き買い物をする</a>
				</div>
				<div class="col-6 center-align">
					<a href="Mypage?id=${userInfo.id}" class="text-danger">ユーザー情報へ</a>
				</div>
			</div>
		</div>
		<div class="col-2 mx-auto">
			<br> </br>
			<h5>購入詳細</h5>
		</div>

		<div class="row col-5 mx-auto">

			<div>
				<div>
					<table border="1">
						<thead>
							<tr>
								<th width="800">購入日時</th>
								<th width="300">合計金額</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="center"><fmt:formatDate value="${tp.buyDate}" type="DATE"
											pattern="yyyy年MM月dd日 HH:mm:ss" /></td>
								<td class="center">${tp.totalPrice}円</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- 詳細 -->
		<div class="col-6 mx-auto">
			<table border="1">
				<thead>
					<tr>
						<th width="300">商品名</th>
						<th width="200">単価</th>
					</tr>
				</thead>
				<tbody>
				
					<c:forEach var="resultBDB" items="${resultBDB}">
						<tr>
							<td>${resultBDB.name}</td>
							<td>${resultBDB.price}円</td>
						</tr>
					</c:forEach>

					<tr>
						<td>送料</td>
						<td>500円</td>
					</tr>
				</tbody>
			</table>
		</div>

	</div>

</body>

</html>