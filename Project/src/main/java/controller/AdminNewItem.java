package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Dao.ItemDao;

@WebServlet("/AdminNewItem")
public class AdminNewItem extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AdminNewItem() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/adminNewItem.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String name = request.getParameter("name");
		String detail = request.getParameter("detail");
		int price = Integer.parseInt(request.getParameter("price"));
		String file_name = request.getParameter("file_name");

		/** 入力漏れがあった場合 **/
		if (name.equals("") || detail.equals("") || price == 0 || file_name.equals("")) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			System.out.println("入力漏れがある");
			// エラーjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		ItemDao ItemDao = new ItemDao();
		ItemDao.registeItem(name, detail, price, file_name);

		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("ItemList");

	}
}