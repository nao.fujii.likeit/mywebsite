package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Beans.ItemDataBeans;
import Dao.ItemDao;

@WebServlet("/AdminItemDetail")
public class AdminItemDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AdminItemDetail() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			
			// URLからGETパラメータとしてIDを受け取る
			String id = request.getParameter("id");

			// 確認用：idをコンソールに出力
			System.out.println(id);
			
			//対象のアイテム情報を取得
			ItemDataBeans itemDetail = ItemDao.getByItemID(id);
			//リクエストパラメーターにセット
			request.setAttribute("itemDetail", itemDetail);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/adminItemDetail.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "商品が表示できませんでした。");
			// エラーjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);
		}
	}
}
