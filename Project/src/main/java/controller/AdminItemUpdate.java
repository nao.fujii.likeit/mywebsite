package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Beans.ItemDataBeans;
import Dao.ItemDao;

@WebServlet("/AdminItemUpdate")
public class AdminItemUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AdminItemUpdate() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		// idを引数にして、idに紐づくユーザ情報を出力する
		ItemDataBeans itemDB = ItemDao.findByItemUpdate(id);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("itemDB", itemDB);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/adminItemUpdate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String itemId = request.getParameter("item_id");
		String itemName = request.getParameter("name");
		String detail = request.getParameter("detail");
		String itemPrice = request.getParameter("price");
		String file_name = request.getParameter("file_name");

		System.out.println(itemId + "," + itemName + "," + itemPrice);

		/** 入力漏れがあった場合 **/
		if (itemName.equals("") || detail.equals("") || itemPrice.equals("")) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力漏れがございます。");
			System.out.println("入力漏れがある");
			// エラーjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);
			return;

		} else if (file_name.equals("")) {
			/** 画像データ以外の場合の更新作業 **/
			ItemDao UpdateDao = new ItemDao();
			UpdateDao.UpdateItem(itemId, itemName, detail, itemPrice);
			
		} else {
			/** それ以外の場合の更新作業 **/
			ItemDao UpdateDao = new ItemDao();
			UpdateDao.AllUpdateItem(itemId, itemName, detail, itemPrice, file_name);
		}

		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("ItemList");
	}
}