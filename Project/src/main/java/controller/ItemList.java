package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.ItemDataBeans;
import Dao.ItemDao;

/**
 * Servlet implementation class ItemListServlet
 */
@WebServlet("/ItemList")
public class ItemList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemList() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ユーザ一覧情報を取得
		HttpSession session = request.getSession();
		ItemDao itemDao = new ItemDao();
		List<ItemDataBeans> itemList = itemDao.findAll();

		// リクエストスコープにユーザ一覧情報をセット
		session.setAttribute("itemList", itemList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/adminAllItem.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 検索処理全般
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String itemName = request.getParameter("itemName");
		String priceStart = request.getParameter("priceStart");
		String priceEnd = request.getParameter("priceEnd");

		/** 検索機能へ **/
		ItemDao FindDao = new ItemDao();
		List<ItemDataBeans> itemList = FindDao.findDate(itemName, priceStart, priceEnd);
		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("itemList", itemList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/adminAllItem.jsp");
		dispatcher.forward(request, response);

	}

}
