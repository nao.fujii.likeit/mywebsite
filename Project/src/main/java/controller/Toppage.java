package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.ItemDataBeans;
import Dao.ItemDao;

/**
 * Servlet implementation class Toppage
 */
@WebServlet("/Toppage")
public class Toppage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Toppage() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {

			//商品情報を取得
			ArrayList<ItemDataBeans> itemList = ItemDao.getRandItem(4);

			//リクエストスコープにセット
			request.setAttribute("itemList", itemList);

			System.out.println(itemList);

			//セッションにsearchWordが入っていたら破棄する
			String searchWord = (String) session.getAttribute("searchWord");
			if (searchWord != null) {
				session.removeAttribute("searchWord");
			}
			
			// Topページのjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/topPage.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "Topページに入れませんでした");
			// エラーjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */

//	protected void doPost(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
		/** 
		 * 検索機能
		 */
		// リクエストパラメータの文字コードを指定
//		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
//		String Searchword = request.getParameter("search_word");

//		HttpSession session = request.getSession();
//		session.setAttribute("Searchword", Searchword);
//		System.out.println(Searchword);

		// ユーザ一覧のサーブレットにリダイレクト
	//	response.sendRedirect("ItemSearch");

//	}
}
