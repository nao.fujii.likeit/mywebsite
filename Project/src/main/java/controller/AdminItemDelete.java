package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Beans.ItemDataBeans;
import Dao.ItemDao;

@WebServlet("/AdminItemDelete")
public class AdminItemDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AdminItemDelete() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		// idを引数にして、idに紐づくユーザ情報を出力する
		ItemDataBeans itemDel = ItemDao.findByDelete(id);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("itemDel", itemDel);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/adminItemDelete.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String itemId = request.getParameter("itemId");

		ItemDao DeleteDao = new ItemDao();
		DeleteDao.ItemDelete(itemId);

		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("ItemList");
	}
}