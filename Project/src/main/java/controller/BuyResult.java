package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.BuyDataBeans;
import Beans.BuyDetailDataBeans;
import Beans.ItemDataBeans;
import Dao.BuyDao;
import Dao.BuyDetailDao;

/**
 * 購入完了画面
 *
 * @author nao
 *
 */
@WebServlet("/BuyResult")
public class BuyResult extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public BuyResult() {
		super();
	}

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {
			// セッションからカート情報を取得
			ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) ItemDataBeans.cutSessionAttribute(session,
					"cart");

			BuyDataBeans bdb = (BuyDataBeans) ItemDataBeans.cutSessionAttribute(session, "bdb");

			// 購入情報を登録
			int buyId = BuyDao.insertBuy(bdb);
			// 購入詳細情報を購入情報IDに紐づけして登録
			for (ItemDataBeans cartInItem : cart) {
				BuyDetailDataBeans bddb = new BuyDetailDataBeans();
				bddb.setBuyId(buyId);
				bddb.setItemId(cartInItem.getItem_id());
				BuyDetailDao.insertBuyDetail(bddb);
			}

			/* ====購入完了ページ表示用==== */
			ArrayList<BuyDataBeans> resultBDB = BuyDao.getBuyDataBeansByBuyId(buyId);
			session.setAttribute("resultBDB", resultBDB);
			
			BuyDataBeans tp = BuyDao.getBuyData(buyId);
			session.setAttribute("tp", tp);

			// 購入アイテム情報
			//ArrayList<ItemDataBeans> buyIDBList = BuyDetailDao.getItemDataBeansListByBuyId(buyId);
			//request.setAttribute("buyIDBList", buyIDBList);

			// 購入完了ページ
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyResult.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errMsg", e.toString());
			// エラーjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);
		}
	}
}
