package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Dao.UserDao;

@WebServlet("/NewUser")
public class NewUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public NewUser() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("login_id");
		String userName = request.getParameter("user_name");
		String password = request.getParameter("password");
		String passwordConf = request.getParameter("passwordConf");
		String realName = request.getParameter("real_name");
		String address = request.getParameter("address");
		String birthDate = request.getParameter("birthDate");

		//パスワードが確認用と不一致の時
		if (!(password.equals(passwordConf))) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "パスワードが一致していません。");
			System.out.println("パスワードが合っていない");
			//エラーjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 入力漏れがあった場合 **/
		if (loginId.equals("") || userName.equals("") || password.equals("") || passwordConf.equals("")
				|| realName.equals("") || address.equals("") || birthDate.equals("")) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容に入力漏れがありました。");
			System.out.println("入力漏れがある");
			// エラーjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 既に登録されているログインIDだった場合 **/
		UserDao loginIdDao = new UserDao();

		if (loginIdDao.findByLoginId(loginId) != null) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "既に利用されているログインIDです。");
			System.out.println("ログインID被ってる");
			// エラーjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** パスワードの暗号化 **/
		UserDao PasslockDao = new UserDao();
		String result = PasslockDao.PassMD5(password);

		/** 新規登録処理 **/
		UserDao InsertuserDao = new UserDao();
		InsertuserDao.InsertUser(0, loginId, userName, result, realName, address, birthDate);

		// ログインのサーブレットにリダイレクト
		response.sendRedirect("Login");

	}
}