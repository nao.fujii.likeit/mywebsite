package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.BuyDataBeans;
import Beans.ItemDataBeans;
import Beans.User;

/**
 * 商品購入画面
 *
 * @author nao
 *
 */
@WebServlet("/Buy")
public class Buy extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション
		HttpSession session = request.getSession();
		try {

			ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

			if (cart.size() == 0) {
				request.setAttribute("cartActionMessage", "購入する商品がありません");
				// エラーjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/error.jsp");
				dispatcher.forward(request, response);

			} else {
				//合計金額
				int total = ItemDataBeans.getTotalItemPrice(cart);

				BuyDataBeans bdb = new BuyDataBeans();
				User user = (User) session.getAttribute("userInfo");
				int userId = user.getId();
				bdb.setUserId(userId);
				//送料を追加
				int totalPrice = total + 500;
				bdb.setTotalPrice(totalPrice);

				//購入確定で利用
				session.setAttribute("bdb", bdb);
				// 購入確認jspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyConfirm.jsp");
				dispatcher.forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errMsg", e.toString());
			// エラーjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);
		}
	}
}
