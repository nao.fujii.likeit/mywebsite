package Beans;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

/**
 * アイテム
 * @author nao
 *
 */
public class ItemDataBeans implements Serializable {
	private int item_id;
	private String name;
	private String detail;
	private int price;
	private String file_name;
	private String createDate;
	private String updateDate;
	
	// 全てのアイテムデータをセットするコンストラクタ
	public ItemDataBeans(int itemId,String itemName, String detail, int itemPrice, String file_name, String createDate, String updateDate) {
		this.item_id = itemId;
		this.name = itemName;
		this.detail = detail;
		this.price = itemPrice;
		this.file_name = file_name;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public ItemDataBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}
	
	// アイテム削除に向けてのコンストラクタ
	public ItemDataBeans(int itemId, String itemName,String file_name) {
		this.item_id = itemId;
		this.name = itemName;
		this.file_name = file_name;
	}

	public int getItem_id() {
		return item_id;
	}

	public void setItem_id(int itemId) {
		this.item_id = itemId;
	}

	public String getName() {
		return name;
	}

	public void setName(String itemName) {
		this.name = itemName;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int itemPrice) {
		this.price = itemPrice;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}
	
	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * 商品の合計金額を算出する
	 *
	 * @param items
	 * @return total
	 */
	public static int getTotalItemPrice(ArrayList<ItemDataBeans> items) {
		int total = 0;
		for (ItemDataBeans item : items) {
			total += item.getPrice();
		}
		return total;
	}

	/**
	 * セッションから指定データを取得（削除も一緒に行う）
	 *
	 * @param session
	 * @param str
	 * @return
	 */
	public static Object cutSessionAttribute(HttpSession session, String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);

		return test;
	}
}
