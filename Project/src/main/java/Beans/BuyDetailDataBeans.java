package Beans;

import java.io.Serializable;


/**
 * 購入詳細
 * @author nao
 *
 */
public class BuyDetailDataBeans  implements Serializable {
	private int id;
	private int itemId;
	private int buyId;


	public int getId() {
		return id;
	}
	public void setId(int buyDetailId) {
		this.id = buyDetailId;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public int getBuyId() {
		return buyId;
	}
	public void setBuyId(int buyId) {
		this.buyId = buyId;
	}

}
