package Beans;

import java.io.Serializable;
import java.util.Date;

/**
 * Userテーブルのデータを格納するためのBeans
 * @author nao
 *
 */
public class User implements Serializable {
	private int user_id;
	private String loginId;
	private String user_name;
	private String password;
	private String real_name;
	private String address;
	private Date birthDate;
	private String createDate;
	private String updateDate;

	// ログインセッションを保存するためのコンストラクタ
	public User(String loginId, String user_name) {
		this.loginId = loginId;
		this.user_name = user_name;
	}

	// ログインセッションを保存・全てのデータをセットするコンストラクタ
	public User(int id, String loginId, String user_name, String password, String real_name, String address,
			Date birthDate, String createDate, String updateDate) {
		this.user_id = id;
		this.loginId = loginId;
		this.user_name = user_name;
		this.password = password;
		this.real_name = real_name;
		this.address = address;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	// IDで検索し詳細データ表示するためのコンストラクタ
	public User(int user_id) {
		this.user_id = user_id;
	}

	// データ更新をする為のコンストラクタ
	public User(String loginId, String user_name, String password,String real_name,String address, String updateDate) {
		this.loginId = loginId;
		this.user_name = user_name;
		this.password = password;
		this.real_name = real_name;
		this.address = address;
		this.updateDate = updateDate;
	}

	// データ削除をする為のコンストラクタ
	public User(String loginId) {
		this.loginId = loginId;
	}

	public int getId() {
		return user_id;
	}

	public void setId(int id) {
		this.user_id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getUserName() {
		return user_name;
	}

	public void setUserName(String user_name) {
		this.user_name = user_name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public String getRealName() {
		return real_name;
	}

	public void setRealName(String real_name) {
		this.real_name = real_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public void setUpdateUserData(String loginId) {
		this.loginId = loginId;
	}
}
