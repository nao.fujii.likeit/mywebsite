package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import Beans.BuyDataBeans;

/**
 *
 * @author nao 
 * 
 */
public class BuyDao {

	/**
	 * 購入情報登録処理
	 * @param bdb 購入情報
	 * @throws SQLException 呼び出し元にスローさせるため
	 */
	public static int insertBuy(BuyDataBeans bdb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		int autoIncKey = -1;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO buy(user_id,total_price,create_date) VALUES(?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			st.setInt(1, bdb.getUserId());
			st.setInt(2, bdb.getTotalPrice());
			st.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
			st.executeUpdate();

			ResultSet rs = st.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}
			System.out.println("購入情報を登録");

			return autoIncKey;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 購入IDによる購入情報検索
	 * @param buyId
	 * @return BuyDataBeans
	 * 				購入情報のデータを持つJavaBeansのリスト
	 * @throws SQLException
	 * 				呼び出し元にスローさせるため
	 */
	public static ArrayList<BuyDataBeans> getBuyDataBeansByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM buy t "
					+ "LEFT JOIN buy_detail t1 ON t.id = t1.buy_id "
					+ "LEFT JOIN item t2 ON t1.item_id = t2.item_id "
					+ "WHERE t.id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();

			ArrayList<BuyDataBeans> resultBDB = new ArrayList<BuyDataBeans>();
			while (rs.next()) {
				BuyDataBeans bdb = new BuyDataBeans();
				bdb.setId(rs.getInt("id"));
				bdb.setTotalPrice(rs.getInt("total_price"));
				bdb.setBuyDate(rs.getTimestamp("create_date"));
				bdb.setUserId(rs.getInt("user_id"));
				bdb.setItem_id(rs.getInt("item_id"));
				bdb.setName(rs.getString("name"));
				bdb.setPrice(rs.getInt("price"));
				resultBDB.add(bdb);
			}

			System.out.println("購入情報を確認");
			return resultBDB;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 購入IDによる購入情報検索
	 * @param buyId
	 * @return BuyDataBeans
	 */
	public static BuyDataBeans getBuyData(int Id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM buy WHERE id = ?");
			st.setInt(1, Id);

			ResultSet rs = st.executeQuery();

			BuyDataBeans tp = new BuyDataBeans();
			if (rs.next()) {
				tp.setId(rs.getInt("id"));
				tp.setUserId(rs.getInt("user_id"));
				tp.setTotalPrice(rs.getInt("total_price"));
				tp.setBuyDate(rs.getTimestamp("create_date"));
			}
			System.out.println("購入時間、総額の確認");
			return tp;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * ユーザIDによる購入情報検索
	 * @param userId
	 * @return BuyDataBeans
	 * 				購入情報のデータを持つJavaBeansのリスト
	 * @throws SQLException
	 * 				呼び出し元にスローさせるため
	 */
	public static List<BuyDataBeans> getUserBuyData(int userid) throws SQLException {
		Connection con = null;
		List<BuyDataBeans> BuyDate = new ArrayList<BuyDataBeans>();
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM t_buy t LEFT OUTER JOIN m_delivery_method t1 ON t.delivery_method_id = t1.id WHERE t.user_id = ?;");
			st.setInt(1, userid);

			ResultSet rs = st.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				BuyDataBeans buydate = new BuyDataBeans();
				buydate.setId(rs.getInt("id"));
				buydate.setUserId(rs.getInt("user_id"));
				buydate.setTotalPrice(rs.getInt("total_price"));
				buydate.setBuyDate(rs.getTimestamp("create_date"));

				BuyDate.add(buydate);

			}
			System.out.println("searching BuyDataBeans by buyID has been completed");
			return BuyDate;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}
