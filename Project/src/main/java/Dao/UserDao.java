package Dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import Beans.User;

public class UserDao {

	/**
	 * ログインIDとパスワードに紐づく全てのユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public User findByLogin(String loginid, String Password) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginid);
			pStmt.setString(2, Password);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}
			// 必要なデータのみインスタンスのフィールドに追加
			int id = rs.getInt("user_id");
			String loginId = rs.getString("login_id");
			String userName = rs.getString("user_name");
			String password = rs.getString("password");
			String realName = rs.getString("real_name");
			String address = rs.getString("address");
			Date birthDate = rs.getDate("birth_date");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			User user = new User(id, loginId, userName, password, realName, address, birthDate, createDate,
					updateDate);

			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public static User findByLoginInfo(String loginId, String password) {
		Connection con = null;
		try {
			// データベースへ接続
			con = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int id = rs.getInt("user_id");
			return  new User(id);

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("繋がってない");
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * パスワードを暗号化する
	 * @param password
	 * @return MD5
	 */
	public String PassMD5(String password) {

		//ハッシュを生成したい元の文字列
		String source = password;

		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;

		//ハッシュアルゴリズム
		String algorithm = "MD5";
		String result = "";
		try {
			//ハッシュ生成処理
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));

			result = DatatypeConverter.printHexBinary(bytes);

		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * ログインIDの被りを確認する
	 * @param loginId
	 * @return
	 */

	public User findByLoginId(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT login_id FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			System.out.println(loginIdData);
			return new User(loginIdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 新規ユーザ情報を登録する
	 * @return
	 */
	public void InsertUser(int id, String loginId, String userName, String password, String realName, String address,
			String birthDate) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String insertSQL = "INSERT INTO user(login_id,user_name,password,real_name,address,birth_date,create_date,update_date) VALUES(?,?,?,?,?,?,now(),now())";
			PreparedStatement pStmt = conn.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			pStmt.setString(1, loginId);
			pStmt.setString(2, userName);
			pStmt.setString(3, password);
			pStmt.setString(4, realName);
			pStmt.setString(5, address);
			pStmt.setString(6, birthDate);

			//*実行するための文章
			int result = pStmt.executeUpdate();

			// 登録に成功した場合、「データの登録に成功しました。」と表示する
			// 登録に失敗した場合、「データの登録に失敗しました。」と表示する

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");
		} finally {
			try {

				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					System.out.println("データの登録に成功しました。");
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 管理者以外の全てのユーザ情報を取得する
	 * @return
	 */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE login_id != 'admin' ";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("user_id");
				String loginId = rs.getString("login_id");
				String userName = rs.getString("user_name");
				String password = rs.getString("password");
				String realName = rs.getString("real_name");
				String address = rs.getString("address");
				Date birthDate = rs.getDate("birth_date");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, userName, password, realName, address, birthDate, createDate,
						updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 * 検索機能 ユーザ情報を取得する
	 * ログインID（完全一致）
	 * ユーザ名（部分一致）
	 * 生年月日（開始日と終了日の範囲内に該当するもの）
	 * @return
	 */
	public List<User> findDate(String loginId, String userName, String dateStart, String dateEnd) {
		Connection conn = null;
		List<User> findList = new ArrayList<User>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id != 'admin'";
			if (!loginId.equals("")) {
				sql += "AND login_id = '" + loginId + "'";
			}
			if (!userName.equals("")) {
				sql += "AND user_name LIKE '" + "%" + userName + "%" + "'";
			}
			if (!dateStart.equals("")) {
				sql += "AND birth_date >=  '" + dateStart + "'";
			}
			if (!dateEnd.equals("")) {
				sql += "AND birth_date <  '" + dateEnd + "'";
			}

			// SELECTを実行し、結果表を取得
			Statement Stmt = conn.prepareStatement(sql);
			ResultSet rs = Stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("user_id");
				String LoginId = rs.getString("login_id");
				String UserName = rs.getString("user_name");
				String password = rs.getString("password");
				String realName = rs.getString("real_name");
				String address = rs.getString("address");
				Date birthDate = rs.getDate("birth_date");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, LoginId, UserName, password, realName, address, birthDate, createDate,
						updateDate);

				findList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return findList;
	}

	/**
	 * ユーザ情報を削除する
	 * @return
	 */

	public static User findByDelete(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE user_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginId = rs.getString("login_id");
			String userName = rs.getString("user_name");
			return new User(loginId, userName);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void UserDelete(String loginId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String insertSQL = "DELETE FROM user WHERE login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			pStmt.setString(1, loginId);

			//*実行するための文章
			int result = pStmt.executeUpdate();

			// 登録に成功した場合、「データ削除しました。」と表示する
			// 登録に失敗した場合、「データの削除に失敗しました。」と表示する

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("データの削除に失敗しました。");
		} finally {
			try {

				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					System.out.println("データ削除しました。");
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * IDが紐づくユーザ詳細情報を返す（String）
	 * @param Id
	 * @return
	 */
	public static User findById(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE user_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータ全てインスタンスのフィールドに追加
			int idDate = rs.getInt("user_id");
			String loginId = rs.getString("login_id");
			String userName = rs.getString("user_name");
			String password = rs.getString("password");
			String realName = rs.getString("real_name");
			String address = rs.getString("address");
			Date birthDate = rs.getDate("birth_date");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			return new User(idDate, loginId, userName, password, realName, address, birthDate, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * LoginIDが紐づくユーザ詳細情報を返す（String）
	 * @param Id
	 * @return
	 */
	public static User findByLoginid(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータ全てインスタンスのフィールドに追加
			int idDate = rs.getInt("user_id");
			String loginId = rs.getString("login_id");
			String userName = rs.getString("user_name");
			String password = rs.getString("password");
			String realName = rs.getString("real_name");
			String address = rs.getString("address");
			Date birthDate = rs.getDate("birth_date");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			return new User(idDate, loginId, userName, password, realName, address, birthDate, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * ユーザ情報を更新する
	 * @param name
	 * @param password
	 * @param birth_date
	 * @return
	 */

	public static User findByUpdate(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE user_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータ全てインスタンスのフィールドに追加
			String loginId = rs.getString("login_id");
			String userName = rs.getString("user_name");
			String password = rs.getString("password");
			String realName = rs.getString("real_name");
			String address = rs.getString("address");
			String updateDate = rs.getString("update_date");
			return new User(loginId, userName, password, realName, address, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void UpdateUser(String loginId, String userName, String password, String realName, String address) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String insertSQL = "UPDATE user SET user_name = ? ,password = ? ,real_name =? ,address = ?,update_date = now() WHERE login_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			pStmt.setString(1, userName);
			pStmt.setString(2, password);
			pStmt.setString(3, realName);
			pStmt.setString(4, address);
			pStmt.setString(5, loginId);

			//*実行するための文章
			int result = pStmt.executeUpdate();

			// 登録に成功した場合、「データの登録に成功しました。」と表示する
			// 登録に失敗した場合、「データの登録に失敗しました。」と表示する

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");

		} finally {
			try {

				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					System.out.println("データの更新に成功しました。");
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * パスワードは変更なしでユーザ情報を更新する
	 * @param name
	 * @param birth_date
	 * @return
	 */
	public void UpdateUserNoPass(String loginId, String userName, String realName, String address) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String insertSQL = "UPDATE user SET user_name = ? ,real_name = ? ,address = ?,update_date = now() WHERE login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			pStmt.setString(1, userName);
			pStmt.setString(2, realName);
			pStmt.setString(3, address);
			pStmt.setString(4, loginId);

			//*実行するための文章
			int result = pStmt.executeUpdate();

			// 登録に成功した場合、「データの登録に成功しました。」と表示する
			// 登録に失敗した場合、「データの登録に失敗しました。」と表示する

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");

		} finally {
			try {

				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					System.out.println("パスワードは更新せず他の項目のみ更新しました。");
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * ユーザーIDを取得
	 *
	 * @param loginId
	 *            ログインID
	 * @param password
	 *            パスワード
	 * @return int ログインIDとパスワードが正しい場合対象のユーザーID 正しくない||登録されていない場合0
	 * @throws SQLException
	 *             呼び出し元にスロー
	 */
	public static int getUserId(String loginId, String password) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM user WHERE login_id = ?");
			st.setString(1, loginId);

			ResultSet rs = st.executeQuery();

			int userId = 0;
			while (rs.next()) {
				if (UserDao.getSha256(password).equals(rs.getString("login_password"))) {
					userId = rs.getInt("user_id");
					System.out.println("login取得");
					break;
				}
			}
			return userId;
		} catch (SQLException e) {
			System.out.println("だめだー");
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * ハッシュ関数
	 *
	 * @param target
	 * @return
	 */
	public static String getSha256(String target) {
		MessageDigest md = null;
		StringBuffer buf = new StringBuffer();
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(target.getBytes());
			byte[] digest = md.digest();

			for (int i = 0; i < digest.length; i++) {
				buf.append(String.format("%02x", digest[i]));
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return buf.toString();
	}

	/**
	 * セッションから指定データを取得（削除も一緒に行う）
	 *
	 * @param session
	 * @param str
	 * @return
	 */
	public static Object cutSessionAttribute(HttpSession session, String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);

		return test;
	}

}
