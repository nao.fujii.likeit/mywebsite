package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Beans.ItemDataBeans;

public class ItemDao {

	/**
	 * 新規アイテム情報を登録する
	 * @return
	 */
	public ItemDataBeans registeItem(String name, String detail, int price, String file_name) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String insertSQL = "INSERT INTO item(name,detail,price,file_name,create_date,update_date) VALUES(?,?,?,?,now(),now())";
			PreparedStatement pStmt = conn.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			pStmt.setString(1, name);
			pStmt.setString(2, detail);
			pStmt.setInt(3, price);
			pStmt.setString(4, file_name);

			//*実行するための文章
			int result = pStmt.executeUpdate();

			// 登録に成功した場合、「データの登録に成功しました。」と表示する
			// 登録に失敗した場合、「データの登録に失敗しました。」と表示する

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");
		} finally {
			try {

				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					System.out.println("データの登録に成功しました。");
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * ランダムで引数指定分のItemDataBeansを取得
	 * @param limit 取得したいかず
	 * @return <ItemDataBeans>
	 * @throws SQLException
	 */
	public static ArrayList<ItemDataBeans> getRandItem(int limit) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM item ORDER BY RAND() LIMIT ? ");
			st.setInt(1, limit);

			ResultSet rs = st.executeQuery();

			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setItem_id(rs.getInt("item_id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFile_name(rs.getString("file_name"));
				itemList.add(item);
			}
			System.out.println("アイテム４つランダム取得");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 全てのアイテム情報を取得する
	 * @return
	 */
	public List<ItemDataBeans> findAll() {
		Connection conn = null;
		List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM item";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int itemId = rs.getInt("item_id");
				String itemName = rs.getString("name");
				String detail = rs.getString("detail");
				int itemPrice = rs.getInt("price");
				String file_name = rs.getString("file_name");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				ItemDataBeans item = new ItemDataBeans(itemId, itemName, detail, itemPrice, file_name,createDate,updateDate);

				itemList.add(item);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}

	/**
	 * 商品IDによる商品検索(int)
	 * @param itemId
	 * @return ItemDataBeans
	 * @throws SQLException
	 */
	public static ItemDataBeans getItemByItemID(int itemId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM item WHERE item_id = ?");
			st.setInt(1, itemId);

			ResultSet rs = st.executeQuery();

			ItemDataBeans item = new ItemDataBeans();
			if (rs.next()) {
				item.setItem_id(rs.getInt("item_id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFile_name(rs.getString("file_name"));
				item.setCreateDate(rs.getString("create_date"));
				item.setUpdateDate(rs.getString("update_date"));
			}

			System.out.println("itemIdで商品検索");

			return item;
		} catch (SQLException e) {
			System.out.println("できてねえ");
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品IDによる商品検索(String)
	 * @param itemId
	 * @return ItemDataBeans
	 * @throws SQLException
	 */
	public static ItemDataBeans getByItemID(String itemId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM item WHERE item_id = ?");
			st.setString(1, itemId);

			ResultSet rs = st.executeQuery();

			ItemDataBeans item = new ItemDataBeans();
			if (rs.next()) {
				item.setItem_id(rs.getInt("item_id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFile_name(rs.getString("file_name"));
				item.setCreateDate(rs.getString("create_date"));
				item.setUpdateDate(rs.getString("update_date"));
			}
			return item;
		} catch (SQLException e) {
			System.out.println("できてねえ");
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品検索
	 * @param searchWord
	 * @param pageNum
	 * @param pageMaxItemCount
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<ItemDataBeans> getItemsByItemName(String searchWord, int pageNum, int pageMaxItemCount)
			throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			if (searchWord.length() == 0) {
				// 全検索
				st = con.prepareStatement("SELECT * FROM item ORDER BY item_id ASC LIMIT ?,? ");
				st.setInt(1, startiItemNum);
				st.setInt(2, pageMaxItemCount);
			} else {
				// 商品名検索
				st = con.prepareStatement("SELECT * FROM item WHERE name LIKE ?  ORDER BY item_id ASC LIMIT ?,? ");
				st.setString(1, "%" + searchWord + "%");
				st.setInt(2, startiItemNum);
				st.setInt(3, pageMaxItemCount);
			}

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setItem_id(rs.getInt("item_id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFile_name(rs.getString("file_name"));
				itemList.add(item);
			}
			System.out.println("itemNameによるアイテムの取得が完了しました");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品総数を取得
	 *
	 * @param searchWord
	 * @return
	 * @throws SQLException
	 */
	public static double getItemCount(String searchWord) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("select count(*) as cnt from item where name like ?");
			st.setString(1, "%" + searchWord + "%");
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * アイテム情報を更新するための情報を取得
	 * @param name
	 * @param password
	 * @param birth_date
	 * @return
	 */

	public static ItemDataBeans findByItemUpdate(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM item WHERE item_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータ全てインスタンスのフィールドに追加
			int itemId = rs.getInt("item_id");
			String itemName = rs.getString("name");
			String detail = rs.getString("detail");
			int itemPrice = rs.getInt("price");
			String file_name = rs.getString("file_name");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			return new ItemDataBeans(itemId,itemName,detail,itemPrice,file_name,createDate,updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	/**
	 * アイテム情報を更新する（画像データ以外）
	 * @param name
	 * @param password
	 * @param birth_date
	 * @return
	 */
	public void UpdateItem(String itemId,String itemName, String detail, String itemPrice) {
		Connection conn = null;
		
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String insertSQL = "UPDATE item SET name = ? ,detail = ? ,price =? ,update_date = now() WHERE item_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			pStmt.setString(1, itemName);
			pStmt.setString(2, detail);
			pStmt.setString(3, itemPrice);
			pStmt.setString(4, itemId);

			//*実行するための文章
			int result = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");

		} finally {
			try {

				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					System.out.println("画像データ以外の更新に成功しました。");
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * 全てのアイテム情報を更新する
	 * @param name
	 * @param password
	 * @param birth_date
	 * @return
	 */
	public void AllUpdateItem(String itemId,String itemName, String detail, String itemPrice, String file_name) {
		Connection conn = null;
		
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String insertSQL = "UPDATE item SET name = ? ,detail = ? ,price =? ,file_name = ?,update_date = now() WHERE item_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			pStmt.setString(1, itemName);
			pStmt.setString(2, detail);
			pStmt.setString(3, itemPrice);
			pStmt.setString(4, file_name);
			pStmt.setString(5, itemId);

			//*実行するための文章
			int result = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");

		} finally {
			try {

				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					System.out.println("データの更新に成功しました。");
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * 検索機能 アイテム情報を取得する
	 * ユーザ名（部分一致）
	 * 金額（開始〜終了の範囲内に該当するもの）
	 * @return
	 */
	public List<ItemDataBeans> findDate(String itemName, String priceStart, String priceEnd) {
		List<ItemDataBeans> findList = new ArrayList<ItemDataBeans>();
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM item WHERE item_id != '0' ";
			if (!itemName.equals("")) {
				sql += "AND name LIKE '%" + itemName + "%' ";
			}
			if (!priceStart.equals("")) {
				sql += "AND price >=  '" + priceStart + "'";
			}
			if (!priceEnd.equals("")) {
				sql += "AND price <  '" + priceEnd + "'";
			}

			// SELECTを実行し、結果表を取得
			Statement Stmt = conn.prepareStatement(sql);
			ResultSet rs = Stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int itemId = rs.getInt("item_id");
				String Name = rs.getString("name");
				String detail = rs.getString("detail");
				int price = rs.getInt("price");
				String file_name = rs.getString("file_name");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				ItemDataBeans itemAllList = new ItemDataBeans(itemId,Name,detail,price,file_name,createDate,updateDate);

				findList.add(itemAllList);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return findList;
	}
	/**
	 * ユーザ情報を削除する
	 * @return
	 */

	public static ItemDataBeans findByDelete(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM item WHERE item_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int itemId = rs.getInt("item_id");
			String itemName = rs.getString("name");
			String file_name = rs.getString("file_name");
			return new ItemDataBeans(itemId, itemName,file_name);
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void ItemDelete(String itemId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String insertSQL = "DELETE FROM item WHERE item_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			pStmt.setString(1, itemId);

			//*実行するための文章
			int result = pStmt.executeUpdate();

			// 登録に成功した場合、「データ削除しました。」と表示する
			// 登録に失敗した場合、「データの削除に失敗しました。」と表示する

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("データの削除に失敗しました。");
		} finally {
			try {

				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					System.out.println("データ削除しました。");
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}


}
