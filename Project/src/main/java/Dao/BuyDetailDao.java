package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Beans.BuyDataBeans;
import Beans.BuyDetailDataBeans;
import Beans.ItemDataBeans;

/**
 *
 * @author nao
 *
 */
public class BuyDetailDao {

	/**
	 * 購入詳細登録処理
	 * @param bddb BuyDetailDataBeans
	 * @throws SQLException
	 * 			呼び出し元にスローさせるため
	 */
	public static void insertBuyDetail(BuyDetailDataBeans bddb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO buy_detail(buy_id,item_id) VALUES(?,?)");
			st.setInt(1, bddb.getBuyId());
			st.setInt(2, bddb.getItemId());
			st.executeUpdate();
			System.out.println("BuyDetail購入詳細を登録");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 購入IDによる購入情報検索
	 * @param buyId
	 * @return {BuyDataDetailBeans}
	 * @throws SQLException
	 */
	public ArrayList<BuyDetailDataBeans> getBuyDataBeansListByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM buy_detail WHERE buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<BuyDetailDataBeans> buyDetailList = new ArrayList<BuyDetailDataBeans>();

			while (rs.next()) {
				BuyDetailDataBeans bddb = new BuyDetailDataBeans();
				bddb.setId(rs.getInt("id"));
				bddb.setBuyId(rs.getInt("buy_id"));
				bddb.setItemId(rs.getInt("item_id"));
				buyDetailList.add(bddb);
			}

			System.out.println("searching BuyDataBeansList by BuyID has been completed");
			return buyDetailList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	* 購入IDによる購入詳細情報検索
	* @param buyId
	* @return buyDetailItemList ArrayList<ItemDataBeans>
	*             購入詳細情報のデータを持つJavaBeansのリスト
	* @throws SQLException
	*/
	public static ArrayList<ItemDataBeans> getItemDataBeansListByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT item.item_id,"
							+ " item.name,"
							+ " item.price"
							+ " FROM buy_detail"
							+ " JOIN item"
							+ " ON buy_detail.item_id = item.item_id"
							+ " WHERE buy_detail.buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> buyDetailItemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setItem_id(rs.getInt("item_id"));
				idb.setName(rs.getString("name"));
				idb.setPrice(rs.getInt("price"));

				buyDetailItemList.add(idb);
			}

			System.out.println("購入IDで購入詳細検索した");
			return buyDetailItemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * IDが紐づくユーザ情報を返す
	 * @param Id
	 * @return
	 */
	public static BuyDetailDataBeans findById(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM buy_detail WHERE buy_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータ全てインスタンスのフィールドに追加
			BuyDetailDataBeans BuyDetailD = new BuyDetailDataBeans();
			BuyDetailD.setBuyId(rs.getInt("buy_id"));
			BuyDetailD.setItemId(rs.getInt("item_id"));
			return BuyDetailD;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 購入IDで購入履歴の詳細情報検索
	 * @param userId
	 * @return BuyDataBeans
	 * 				購入情報のデータを持つJavaBeansのリスト
	 * @throws SQLException
	 * 				呼び出し元にスローさせるため
	 */
	public static List<BuyDataBeans> getBuyHistoryData(String id) throws SQLException {
		Connection con = null;
		List<BuyDataBeans> BuyDate = new ArrayList<BuyDataBeans>();
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM buy t "
					+ "LEFT JOIN buy_detail t1 ON t.id = t1.buy_id "
					+ "LEFT JOIN item i ON t1.item_id = i.item_id "
					+ "WHERE t.user_id = ?;");
			st.setString(1, id);

			ResultSet rs = st.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				BuyDataBeans buydateDetail = new BuyDataBeans();
				buydateDetail.setId(rs.getInt("id"));
				buydateDetail.setUserId(rs.getInt("user_id"));
				buydateDetail.setTotalPrice(rs.getInt("total_price"));
				buydateDetail.setBuyDate(rs.getTimestamp("create_date"));
				buydateDetail.setBuy_id(rs.getInt("buy_id"));
				buydateDetail.setItem_id(rs.getInt("item_id"));
				buydateDetail.setName(rs.getString("name"));
				buydateDetail.setPrice(rs.getInt("price"));
				buydateDetail.setFile_name(rs.getString("file_name"));

				BuyDate.add(buydateDetail);

			}
			System.out.println("購入履歴！");
			return BuyDate;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * アイテムIDで購入商品の詳細情報検索
	 * @param userId
	 * @return BuyDataBeans
	 * 				購入情報のデータを持つJavaBeansのリスト
	 * @throws SQLException
	 * 				呼び出し元にスローさせるため
	 */
	public static List<ItemDataBeans> getBuyHistoryitem(int id) throws SQLException {
		Connection con = null;
		List<ItemDataBeans> ItemDate = new ArrayList<ItemDataBeans>();
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM  buy_detail t INNER JOIN item t1 ON t.item_id = t1.item_id WHERE t.buy_id = ?;");
			st.setInt(1, id);

			ResultSet rs = st.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加

			while (rs.next()) {
				ItemDataBeans itemdateDetail = new ItemDataBeans();
				itemdateDetail.setItem_id(rs.getInt("id"));
				itemdateDetail.setName(rs.getString("name"));
				itemdateDetail.setDetail(rs.getString("detail"));
				itemdateDetail.setPrice(rs.getInt("price"));
				itemdateDetail.setDetail(rs.getString("file_name"));
				
				ItemDate.add(itemdateDetail);
			}
			System.out.println("searching BuyDataBeans by buyID has been completed");
			return ItemDate;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}
